<?php

declare(strict_types=1);

namespace Drupal\simple_oauth_login_hook\EventSubscriber;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Url;
use Drupal\simple_oauth\Authentication\TokenAuthUser;
use Drupal\simple_oauth\Authentication\TokenAuthUserInterface;
use Drupal\simple_oauth\Entity\Oauth2TokenInterface;
use Lcobucci\JWT\Encoding\JoseEncoder;
use Lcobucci\JWT\Token\Parser;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Subscribe to kernel events.
 */
class KernelEventSubscriber implements EventSubscriberInterface {

  /**
   * Construct a new instance.
   */
  public function __construct(
    protected ModuleHandlerInterface $moduleHandler,
    protected EntityTypeManagerInterface $entityTypeManager,
    protected LoggerInterface $logger,
    protected array $loginGrants,
    protected array $moduleDenylist,
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      KernelEvents::RESPONSE => 'onKernelResponse',
    ];
  }

  /**
   * Handle kernel response event.
   */
  public function onKernelResponse(ResponseEvent $event): void {
    $tokenPath = Url::fromRoute('oauth2_token.token')->toString();

    // Only handle requests to /oauth/token.
    if ($event->getRequest()->getPathInfo() !== $tokenPath) {
      return;
    }

    // Do not handle error responses.
    if ($event->getResponse()->getStatusCode() !== 200) {
      return;
    }

    // Get grant type from request.
    $grantType = $event->getRequest()->get('grant_type');
    if (!$grantType) {
      $this->logger->error('Could not get grant type from successful token request!');
      return;
    }

    // If the grant is not in the simple_oauth_login_hook.login_grants
    // service parameter, we do not consider that grant a "login".
    if (!in_array($grantType, $this->loginGrants)) {
      return;
    }

    $account = $this->retrieveAccountFromToken($event);

    $this->setLastLoginTime($account);
    $this->invokeUserLoginHook($account);
  }

  /**
   * Set last login time on user.
   *
   * This implementation is taken from the user module.
   *
   * @see user_login_finalize()
   */
  public function setLastLoginTime(TokenAuthUserInterface $account) {
    $account->setLastLoginTime(\Drupal::time()->getRequestTime());
    \Drupal::entityTypeManager()
      ->getStorage('user')
      ->updateLastLoginTimestamp($account);
  }

  /**
   * Invokes user login hook.
   *
   * Invokes the hook_user_module() for each module implementing
   * this hook that is not in the denylist.
   *
   * @see hook_user_module()
   * @see %simple_oauth_login_hook.user_login_hook_module_denylist%
   */
  protected function invokeUserLoginHook(TokenAuthUserInterface $account) {
    // Invoke hook in modules.
    $this->moduleHandler->invokeAllWith('user_login', function (callable $hook, string $module) use ($account) {
      // Invoke hook if module is not in denylist.
      if (!in_array($module, $this->moduleDenylist)) {
        $hook($account);
      }
    });
  }

  /**
   * Retrieves the account from the access token.
   */
  protected function retrieveAccountFromToken(ResponseEvent $event): ?TokenAuthUserInterface {
    try {
      $tokenResponse = $event->getResponse()->getContent();
      $data = Json::decode($tokenResponse);
      $accessToken = $data['access_token'];

      $token = $this->getOauthTokenFromJwt($accessToken);
      return new TokenAuthUser($token);
    }
    catch (\Throwable $e) {
      $this->logger->critical('Could not get user from access token: @exception', [
        '@exception' => $e->getMessage(),
      ]);
    }

    return NULL;
  }

  /**
   * Retrieves the oauth2 token entity from the JWT string.
   */
  protected function getOauthTokenFromJwt(string $tokenValue): ?Oauth2TokenInterface {
    $parser = new Parser(new JoseEncoder());

    /** @var \Lcobucci\JWT\Token\Plain $token */
    $token = $parser->parse($tokenValue);
    $tokenId = $token->claims()->get('jti');

    $tokens = $this->entityTypeManager->getStorage('oauth2_token')->loadByProperties([
      'value' => $tokenId,
    ]);
    $token = reset($tokens);

    return $token;
  }

}
