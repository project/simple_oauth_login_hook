# Simple OAuth Login Hook

## Contents of this file

 - [Introduction](#introduction)
 - [Requirements](#requirements)
 - [Installation](#installation)
 - [Configuration](#configuration)
 - [Troubleshooting](#troubleshooting)
 - [Maintainers](#maintainers)

## Introduction

The Simple OAuth Login Hook module extends the Simple OAuth module so that
when a user logs in via specific OAuth grants (e.g. the password grant), the
login is properly handled as such by drupal.

This is done by invoking the `user_login()` hook and by updating the `login`
timestamp on the user's entity.

- For a full description of the module, visit the [project page](https://www.drupal.org/project/simple_oauth_login_hook).
- Use the [Issue queue](https://www.drupal.org/project/issues/simple_oauth_login_hook) to submit bug reports and feature suggestions, or track changes.

## Requirements

This module requires Drupal 10 and the following modules:

- [Simple OAuth (Version ^6)](https://www.drupal.org/project/simple_oauth)

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

### Configuration

Some aspects of this module can be configured:

- Which gant types are considered a "login"
- List of modules for which the `user_login()` hook should not be invoked.

Configuration is done through **service parameters**:

```yaml
parameters:
  # Update last login timestamp and invoke hook_user_login() only
  # for the following successful grant types.
  simple_oauth_login_hook.login_grants:
    - 'password'
  # Do not invoke the hook_user_login() for the following modules.
  simple_oauth_login_hook.user_login_hook_module_denylist:
    - 'some_module'
    - 'some_other_module'
```

## Troubleshooting

The grant type string for the 'simple_oauth_login_hook.login_grants' parameter
must be the same as the string used when calling the `/oauth/token` endpoint in
the `grant_type` parameter.

If a module which implements `hook_user_login()` does not work properly, you can
try to put the conflicting module in the
`simple_oauth_login_hook.user_login_hook_module_denylist` parameter.

## Maintainers

Current maintainers:
- Christoph Niedermoser ([@nimoatwoodway](https://www.drupal.org/u/nimoatwoodway))
- Christian Foidl ([@chfoidl](https://www.drupal.org/u/chfoidl))
