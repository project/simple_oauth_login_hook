<?php

namespace Drupal\Tests\simple_oauth_login_hook\Kernel;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\Url;
use Drupal\Tests\simple_oauth\Kernel\AuthorizedRequestBase;
use Drupal\user\Entity\User;
use Symfony\Component\HttpFoundation\Request;

/**
 * Test functionality with enabled grant but denied module.
 */
class EnabledGrantDeniedModuleTest extends AuthorizedRequestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'simple_oauth_login_hook',
    'simple_oauth_login_hook_test',
  ];

  /**
   * The URL to the resource.
   *
   * @var \Drupal\Core\Url
   */
  protected $url;

  /**
   * The kernel.
   *
   * @var \Symfony\Component\HttpKernel\HttpKernelInterface
   */
  protected $httpKernel;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->url = Url::fromRoute('oauth2_token.token');
    $this->httpKernel = $this->container->get('http_kernel');

    $this->client->set('user_id', $this->user)->save();
  }

  /**
   * {@inheritdoc}
   */
  public function register(ContainerBuilder $container) {
    $container->setParameter('simple_oauth_login_hook.login_grants', ['client_credentials']);
    $container->setParameter('simple_oauth_login_hook.user_login_hook_module_denylist', ['simple_oauth_login_hook_test']);

    parent::register($container);
  }

  /**
   * Tests hook invokation.
   */
  public function testHookInvokation() {
    $parameters = [
      'grant_type' => 'client_credentials',
      'client_id' => $this->client->getClientId(),
      'client_secret' => $this->clientSecret,
      'scope' => $this->scope,
    ];

    // Last login time is 0.
    $this->assertEquals(0, $this->user->getLastLoginTime());

    // Hook has not been invoked.
    $hookPayload = \Drupal::state()->get('user_login_hook_invoked');
    $this->assertNull($hookPayload);

    $request = Request::create($this->url->toString(), 'POST', $parameters);
    $this->httpKernel->handle($request);

    $updatedUser = User::load($this->user->id());

    // Last login time is updated.
    $this->assertEquals(
      $request->server->get('REQUEST_TIME'),
      $updatedUser->getLastLoginTime()
    );

    // Hook has been invoked with user.
    $hookPayload = \Drupal::state()->get('user_login_hook_invoked');
    $this->assertNull($hookPayload);
  }

}
